﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public float waitToRespawn;
	public PlayerController thePlayer;
	public GameObject deathExplosion;

	public int coinCount;
	public Text coinText;
	public Image carrot1;
	public Image carrot2;
	public Image carrot3;

	public Sprite fullCarrot; //The Golden one
	public Sprite halfCarrot; //The natural one
	public Sprite emptyCarrot; //The bubble

	public int maxHealth;
	public int healthCount;

	public bool respawning;

	public ResetOnRespawn[] objectsToReset;

	public bool invincible;

	public Text livesText;
	public int startingLives;
	public int currentLives;

	public GameObject gameOverScreen;

	public AudioSource coinEffect;

	public bool once = true;
	AccountsManager theAccountManager;
	public int loadedCoins;
	public int loadedhealth;
	public int loadedLives;

	// Use this for initialization
	void Start () {
		thePlayer = FindObjectOfType<PlayerController> ();
		theAccountManager = FindObjectOfType<AccountsManager> ();
		objectsToReset = FindObjectsOfType<ResetOnRespawn> ();
		loadedCoins = 0;
		loadedhealth = 0;
		loadedLives = 0;
		coinText.text = "Coins: "+coinCount;
		healthCount = maxHealth;
		currentLives = startingLives;
		livesText.text = "Lives X " + currentLives;
	}
	
	// Update is called once per frame
	void Update () {

		if (once) { //This is to start the game from an old progress if exists (Loading)
			Debug.Log ("Initial ID id " + thePlayer.playerID);
			theAccountManager.LoadProgress (thePlayer.playerID);
			loadedCoins = theAccountManager.coins;
			loadedhealth = theAccountManager.health;
			loadedLives = theAccountManager.lives;
			if (loadedCoins != 0) {
				coinCount = loadedCoins;
				coinText.text = "Coins: "+coinCount;
			}
			if (loadedhealth != 0) {
				healthCount = loadedhealth;
				UpdateHeartMeter ();
			}
			if (loadedLives != 0) {
				currentLives = loadedLives;
				livesText.text = "Lives X " + currentLives;
			}
			once = false;
		}

		if (healthCount <= 0 && !respawning){
			Respawn ();
			respawning = true;
		}
		//Debug.Log (healthCount, gameObject);
	}

	public void Respawn (){
		Debug.Log ("Respawning", gameObject);
		currentLives -= 1;
		livesText.text = "Lives X " + currentLives;

		if (currentLives > 0) {
			StartCoroutine ("RespawnCo"); //It searches coroutines within this script
		} else {
			thePlayer.gameObject.SetActive (false);
			Instantiate (deathExplosion, thePlayer.transform.position, thePlayer.transform.rotation);
			gameOverScreen.SetActive (true);
		}

	}

	public void Revival (){
		theAccountManager.RemoveAllProgress (thePlayer.playerID);
		theAccountManager.RemoveAllChoices (thePlayer.playerID);
		SceneManager.LoadScene ("Level1");
	}

	public IEnumerator RespawnCo(){ //A coroutine allows creating delays without crashing the game
		thePlayer.gameObject.SetActive (false);
		Instantiate (deathExplosion, thePlayer.transform.position, thePlayer.transform.rotation);
		yield return new WaitForSeconds (waitToRespawn);

		healthCount = maxHealth;
		thePlayer.transform.parent = null; //Fixes a bug where the player sometimes keeps having a moving parent due to being on one in the last second of the game
		respawning = false;
		UpdateHeartMeter ();

//		coinCount = 0;
//		coinText.text = "Coins: "+coinCount;

		thePlayer.transform.position = thePlayer.respawnPosition;
		thePlayer.gameObject.SetActive (true);

//		for (int i=0; i<objectsToReset.Length; i++) {
//			objectsToReset [i].gameObject.SetActive (true);
//			objectsToReset [i].ResetObject ();
//		}
		thePlayer.die = true;
	}
		

	public void addCoins(int coinsToAdd){
		coinEffect.Play ();
		coinCount += coinsToAdd; //cointCount=cointCount+coinsToAdd
		coinText.text = "Coins: "+coinCount;
		//Debug.Log(cointCount, gameObject);
	}

	public void HurtPlayer(int damageToTake){

		if (!invincible) {
			thePlayer.hurtEffect.Play ();
			healthCount -= damageToTake;
			UpdateHeartMeter ();
			thePlayer.Knockback ();
		}
	}

	public void GiveHealth (int healthToGive) {
		coinEffect.Play ();
		healthCount += healthToGive;
		if (healthCount > maxHealth) {
			healthCount = maxHealth;
		}

		UpdateHeartMeter ();
	}

	public void UpdateHeartMeter(){
		switch(healthCount){
		case 6:
			carrot1.sprite = fullCarrot;
			carrot2.sprite = fullCarrot;
			carrot3.sprite = fullCarrot;
			return;

		case 5:
			carrot1.sprite = fullCarrot;
			carrot2.sprite = fullCarrot;
			carrot3.sprite = halfCarrot;
			return;

		case 4:
			carrot1.sprite = fullCarrot;
			carrot2.sprite = fullCarrot;
			carrot3.sprite = emptyCarrot;
			return;

		case 3:
			carrot1.sprite = fullCarrot;
			carrot2.sprite = halfCarrot;
			carrot3.sprite = emptyCarrot;
			return;

		case 2:
			carrot1.sprite = fullCarrot;
			carrot2.sprite = emptyCarrot;
			carrot3.sprite = emptyCarrot;
			return;

		case 1:
			carrot1.sprite = halfCarrot;
			carrot2.sprite = emptyCarrot;
			carrot3.sprite = emptyCarrot;
			return;

		case 0:
			carrot1.sprite = emptyCarrot;
			carrot2.sprite = emptyCarrot;
			carrot3.sprite = emptyCarrot;
			return;

		default:
			carrot1.sprite = emptyCarrot;
			carrot2.sprite = emptyCarrot;
			carrot3.sprite = emptyCarrot;
			return;
		}
	}

	public void AddLives (int livesToAdd) {
		coinEffect.Play ();
		currentLives += livesToAdd;
		livesText.text = "Lives X " + currentLives;
	}
}
