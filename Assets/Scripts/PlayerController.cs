﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	public float jumpSpeed;
	private Rigidbody2D myRigidBody;

	public Transform checkGround;
	public float checkGroundRadius;
	public LayerMask whatIsGround;
	public bool isGrounded;

	private Animator myAnim;

	public Vector3 respawnPosition;

	public LevelManager theLevelManager;

	public GameObject stompBox;

	public float knockbackForce;
	public float knockbackLength;
	public float knockbackCounter;

	public AudioSource jumpEffect;
	public AudioSource hurtEffect;

	public bool canMove;
	//private SpriteRenderer theRenderer;
	//public Sprite whenTalkingSprite;

	public AccountsManager theAccountManager;
	public float loadedX;
	public float loadedY;
	public float loadedZ;
	public string playerID;
	public string playerEmail;
	public bool once;
	public float xToSave;
	public float yToSave;
	public float zToSave;
	public int coinsToSave;
	public int healthToSave;
	public int livesToSave;
	public ArrayList choicesToSave = new ArrayList();
	public RESTManager theRESTManager;
	public bool die;

	// Use this for initialization
	void Start () {
		myRigidBody = GetComponent<Rigidbody2D> (); //Creating an instance of the class Rigidbody2D attached to the player pbject
		myAnim = GetComponent<Animator> ();
		theAccountManager = FindObjectOfType<AccountsManager> ();
		theRESTManager = FindObjectOfType<RESTManager> ();
		//playerID = PlayerPrefs.GetString ("playerID");
		playerID = "12";
		loadedX = 0.0f;
		loadedY = 0.0f;
		loadedZ = 0.0f;
		once = true;
		die = true;
		respawnPosition = transform.position; //Only if you're just starting the game
		theLevelManager = FindObjectOfType<LevelManager> (); //Interacting with other objects of different classes
	}
	
	// Update is called once per frame
	void Update () {

		if (once) { //This is to start the game from an old progress if exists (Loading)
			theAccountManager.LoadProgress (playerID);
			loadedX = theAccountManager.positionX;
			loadedY = theAccountManager.positionY;
			loadedZ = theAccountManager.positionZ;
			//Debug.Log ("Initial Coins is " + loadedCoins);
			if (loadedX != 0) {
				transform.position = new Vector3 (loadedX, loadedY, loadedZ);
				respawnPosition = transform.position; //In case you've any saved progress
			} 
			once = false;
		}

		//Debug.Log(canMove, gameObject);
		if (!canMove) {
			myRigidBody.velocity = new Vector3 (0f, myRigidBody.velocity.y, 0f);
			//myAnim.PlayInFixedTime("Kareem Conv");
			//myAnim.StartPlayback ();
			myAnim.Play ("Kareem Conv");
			//myAnim.speed = System.Convert.ToSingle(0.5);
			return;
		}

		isGrounded = Physics2D.OverlapCircle (checkGround.position, checkGroundRadius, whatIsGround);

		//#if UNITY_STANDALONE

		if (knockbackCounter <= 0) {

			if (Input.GetAxisRaw ("Horizontal") > 0f) {
				myRigidBody.velocity = new Vector3 (moveSpeed, myRigidBody.velocity.y, 0f);
				transform.localScale = new Vector3 (0.2785499f, 0.2906694f, 1f);
			} else if (Input.GetAxisRaw ("Horizontal") < 0f) {
				myRigidBody.velocity = new Vector3 (-moveSpeed, myRigidBody.velocity.y, 0f);
				transform.localScale = new Vector3 (-0.2785499f, 0.2906694f, 1f);
			} else {
				myRigidBody.velocity = new Vector3 (0f, myRigidBody.velocity.y, 0f);
			}
			if (Input.GetButtonDown ("Jump") && isGrounded) {
				myRigidBody.velocity = new Vector3 (myRigidBody.velocity.x, jumpSpeed, 0f);
				jumpEffect.Play ();
			}
			theLevelManager.invincible = false;
		}
		//#endif
		if (knockbackCounter > 0) {
			knockbackCounter -= Time.deltaTime;

			if (transform.localScale.x > 0) {
				myRigidBody.velocity = new Vector3 (-knockbackForce, knockbackForce, 0f);
			} else {
				myRigidBody.velocity = new Vector3 (knockbackForce, knockbackForce, 0f);
			}
		}

		myAnim.SetFloat ("Speed", Mathf.Abs (myRigidBody.velocity.x));
		myAnim.SetBool ("Grounded", isGrounded);

		if (myRigidBody.velocity.y < 0) { //Making sure the stompBox is only active while jumping
			stompBox.SetActive (true);
		} else {
			stompBox.SetActive (false);
		}

		}

	public void Knockback () {
		knockbackCounter = knockbackLength;
		theLevelManager.invincible = true;
	}

	void OnTriggerEnter2D (Collider2D other){ //Starts when existing in a trigger zone (box or circle)
		if (other.tag == "DeathZone") {
			//gameObject.SetActive (false);
			Debug.Log ("dead", gameObject);
			//transform.position = respawnPosition;
			if (die) {
				die = false;
				theLevelManager.Respawn ();
			}
		} else if (other.tag == "Checkpoint"){
			Debug.Log ("Checkpoint reached", gameObject);
			respawnPosition = other.transform.position;
			//Saving Checkpoint progress in the local DB
			xToSave = other.transform.position.x;
			yToSave = other.transform.position.y;
			zToSave = other.transform.position.z;
			coinsToSave = theLevelManager.coinCount;
			healthToSave = theLevelManager.healthCount;
			livesToSave = theLevelManager.currentLives;

			theAccountManager.SaveProgress (playerID, xToSave, yToSave, zToSave, coinsToSave, healthToSave, livesToSave);
			foreach (int i in choicesToSave) {
				theAccountManager.SaveChoicesProgress (playerID, i);
			}
			//Clearing the choices list to get reused
			choicesToSave.Clear();
				
			//Sending Progress & Choices to the cloud
			//theRESTManager.uploadProgress (playerEmail, System.Convert.ToDouble (xToSave), System.Convert.ToDouble (yToSave), 
			//	System.Convert.ToDouble (zToSave), coinsToSave, healthToSave, livesToSave);

			//Ferch all choices from the local DB
			choicesToSave = theAccountManager.FetchAllChoices(playerID);
			theRESTManager.UploadChoices (playerID, choicesToSave);

			//Clearing the choices list to get reused
			choicesToSave.Clear();
		}
	}

	void OnCollisionEnter2D (Collision2D other){ //Starts on colliding with another object
		if (other.gameObject.tag == "floating"){ //Using gameObject is needed because there's no class called Collision2D
			transform.parent = other.transform;  //Setting the flaot a parent to our Player
		}
	}

	void OnCollisionExit2D (Collision2D other){
		if (other.gameObject.tag == "floating"){
			transform.parent = null; //Nullifying the parenthood of the float to our Player
		}
	}



	public void TouchMove (float moveInput) {

		isGrounded = Physics2D.OverlapCircle (checkGround.position, checkGroundRadius, whatIsGround);

		if (knockbackCounter <= 0) {

			if (moveInput > 0f) {
				Debug.Log ("Moving Right!");
				myRigidBody.velocity = new Vector3 (moveSpeed, myRigidBody.velocity.y, 0f);
				transform.localScale = new Vector3 (0.2785499f, 0.2906694f, 1f);
			} else if (moveInput < 0f) {
				myRigidBody.velocity = new Vector3 (-moveSpeed, myRigidBody.velocity.y, 0f);
				transform.localScale = new Vector3 (-0.2785499f, 0.2906694f, 1f);
			} else {
				myRigidBody.velocity = new Vector3 (0f, myRigidBody.velocity.y, 0f);
			}

			theLevelManager.invincible = false;
		}

	}

	public void TouchJump () {

		isGrounded = Physics2D.OverlapCircle (checkGround.position, checkGroundRadius, whatIsGround);
		
		if (knockbackCounter <= 0 && isGrounded) {
			myRigidBody.velocity = new Vector3 (myRigidBody.velocity.x, jumpSpeed, 0f);
			jumpEffect.Play ();
		}

		theLevelManager.invincible = false;

	}

}
