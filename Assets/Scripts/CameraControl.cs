﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

	public GameObject target;
	public float followAhead;
	private Vector3 targetPosition;
	public float smoothing;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		targetPosition = new Vector3 (target.transform.position.x, transform.position.y, transform.position.z);
		if (target.transform.localScale.x > 0f) {
			targetPosition = new Vector3 (targetPosition.x + followAhead, targetPosition.y, targetPosition.z);
		} else {
			targetPosition = new Vector3 (targetPosition.x + followAhead, targetPosition.y, targetPosition.z);
		}
		//transform.position = targetPosition;
		//Lerp is used to make transition between the given 2 positions smooth
		transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing*Time.deltaTime);
		//deltaTime is the duration of 1 frame, by default 1/60S with slight changes
	}
}
