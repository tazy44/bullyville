﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExistingPlayer {
	
	public string id { get; set; }
	public string email { get; set; }
	public string password { get; set; }
	public int age { get; set; }
	public string firstName { get; set; }
	public string lastName { get; set; }

	public ExistingPlayer (string id, string email, string password, string fN, string lN, int age) {

		this.id = id;
		this.email = email;
		this.password = password;
		this.age = age;
		this.firstName = fN;
		this.lastName = lN;

	}

}
