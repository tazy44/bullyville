﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
//using System;

public class RESTManager : MonoBehaviour {

	string ID; //To append to the Get request;

	// Use this for initialization
	void Start () {
		//StartCoroutine ("ReceiveData");
	}
	
	// Update is called once per frame
	void Update () {



	}

	public void SendNewAccountToCloud (string FN, string LN, int age, string email, string password, string ID) {


		StartCoroutine(WaitForRequest( FN,  LN,  age, email, password, ID));

	}

	public IEnumerator WaitForRequest(string FN, string LN, int age, string email, string password, string ID)
	{

		//Sending a form-format POST request to a PHP file
		WWWForm form = new WWWForm ();
		form.AddField ("firstName", FN);
		form.AddField ("lastName", LN);
		form.AddField ("age", age);
		form.AddField ("email", email);
		form.AddField ("password", password);
		form.AddField ("id", ID);
		Debug.Log ("Right before request");
		//WWW poster = new WWW ("http://bullyville.rf.gd/postnewaccount.php", form);
		//WWW poster = new WWW ("https://lit-hollows-39194.herokuapp.com/postnewaccount.php", form);
		WWW poster = new WWW ("http://localhost/bv-php/postnewaccount.php", form);

		yield return poster;

			// check for errors
		if (poster.error == null)
			{
			Debug.Log("WWW Ok!: " + poster.data);
			} else {
			Debug.Log("WWW Error: "+ poster.error);
			}    
	}  

	public IEnumerator ReceiveData() {

		ID = "590751cb24b1660011417d63";

		WWW itemsData = new WWW ("https://obscure-basin-39459.herokuapp.com/api/players/"+ID);
		yield return itemsData;
		string itemsDataString = itemsData.text;
		Debug.Log(itemsDataString);

	}

	public void UploadChoices (string playerID, ArrayList choices) {
		
		//Sending a form-format POST request to a PHP file
		WWWForm form = new WWWForm ();
		form.AddField ("id", playerID);
		string choicesInString = "";
		foreach (int i in choices) {
			i.ToString ();
			choicesInString = choicesInString+i+'-';
		}

		Debug.Log (choicesInString);
		Debug.Log (playerID);
		form.AddField ("choices", choicesInString);
		WWW poster = new WWW ("http://localhost/bv-php/postchoices.php", form);

	}

}

