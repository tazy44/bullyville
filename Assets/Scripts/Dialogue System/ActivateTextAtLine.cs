﻿using UnityEngine;
using System.Collections;

public class ActivateTextAtLine : MonoBehaviour {

	public TextAsset theText;
	public int startLine;
	public int endLine;
	public DialogueManager theDialogueManager;
	public bool destroyWhenFinished;

	public bool requireButtonPress;
	public bool waitForPress;

	public int[] playerLines; //Lines the player says
	public int[] otherLines; //Lines the side character says
	public int[] optionLines; //Lines that activates otions and stops the conversation
	public int[] options; //Lines that include the options displayed on buttons
	public int[] branches; //Lines you go to based on the options selected in the conversation
	public int[] killerLines; //The lines that just kill conversations (they end conversations)

	// Use this for initialization
	void Start () {

		theDialogueManager = FindObjectOfType<DialogueManager> ();

	}
	
	// Update is called once per frame
	void Update () {

		if (waitForPress && Input.GetKeyDown (KeyCode.J)) {
			theDialogueManager.ReloadScript (theText);
			theDialogueManager.currentLine = startLine;
			theDialogueManager.endAtLine = endLine;
			theDialogueManager.playerLines = playerLines;
			theDialogueManager.otherLines = otherLines;
			theDialogueManager.optionLines = optionLines;
			theDialogueManager.options = options;
			theDialogueManager.branches = branches;
			theDialogueManager.killerLines = killerLines;
			theDialogueManager.EnableTextBox();
			if (destroyWhenFinished) {
				Destroy (gameObject);
			}
		}

	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "Player") {

			if (requireButtonPress) {
				waitForPress = true;
				return;
			}

			theDialogueManager.ReloadScript (theText);
			theDialogueManager.currentLine = startLine;
			theDialogueManager.endAtLine = endLine;
			theDialogueManager.playerLines = playerLines;
			theDialogueManager.otherLines = otherLines;
			theDialogueManager.optionLines = optionLines;
			theDialogueManager.options = options;
			theDialogueManager.branches = branches;
			theDialogueManager.killerLines = killerLines;
			theDialogueManager.EnableTextBox();
			if (destroyWhenFinished) {
				Destroy (gameObject);
			}
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		if (other.tag == "Player") {
			waitForPress = false;
		}
	}
}
