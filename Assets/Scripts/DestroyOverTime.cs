﻿using UnityEngine;
using System.Collections;

public class DestroyOverTime : MonoBehaviour {

	public float lifeTime;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		lifeTime = lifeTime - Time.deltaTime; //deltaTime is the duration of 1 frame, by default 1/60S with slight changes
		if (lifeTime <= 0f){
			Destroy (gameObject);
		}
	}
}
