﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControls : MonoBehaviour {

	private PlayerController thePlayer;
	private DialogueManager theDialogue;

	// Use this for initialization
	void Start () {
		thePlayer = FindObjectOfType<PlayerController> ();
		theDialogue = FindObjectOfType<DialogueManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void RightArrow () {
		thePlayer.TouchMove (10);
	}

	public void LeftArrow () {
		thePlayer.TouchMove (-10);
	}

	public void JumpArrow () {
		thePlayer.TouchJump ();
	}

	public void UnpressArrow () {
		thePlayer.TouchMove (0);
	}

//	public void PressEnter () {
//		theDialogue.NextLine ();
//	}

}
