﻿using UnityEngine;
using System.Collections;

public class PatrollingEnemy : MonoBehaviour {

	public Transform rightPoint;
	public Transform leftPoint;
	public bool movingRight;
	public float moveSpeed;
	public Rigidbody2D myRigidbody;

	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (movingRight && transform.position.x > rightPoint.position.x) {
			movingRight = false;
		}
		if (!movingRight && transform.position.x < leftPoint.position.x) {
			movingRight = true;
		}
		if (movingRight) {
			myRigidbody.velocity = new Vector3 (moveSpeed, myRigidbody.velocity.y, 0f);
		}
		if (!movingRight) {
			myRigidbody.velocity = new Vector3 (-moveSpeed, myRigidbody.velocity.y, 0f);
		}
	}
}
