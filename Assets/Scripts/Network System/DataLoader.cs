﻿using UnityEngine;
using System.Collections;

public class DataLoader : MonoBehaviour {

	public string[] items;

	// Use this for initialization
	void Start () {

		StartCoroutine ("ReceiveData");

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public IEnumerator ReceiveData() {
	
		WWW itemsData = new WWW ("https://obscure-basin-39459.herokuapp.com/api/players");
		yield return itemsData;
		string itemsDataString = itemsData.text;
		//print (itemsDataString);
		//items = itemsDataString.Split (';');
		//print (GetDataValue(items[0], "Time:"));
		Debug.Log(itemsDataString);
	
	}

	string GetDataValue(string data, string index) {
		
		string value = data.Substring (data.IndexOf(index)+index.Length);
		if (value.Contains ("|")) {
			value = value.Remove (value.IndexOf ("|"));
		}
		return value;

	}
}
