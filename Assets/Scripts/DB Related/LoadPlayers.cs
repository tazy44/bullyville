﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LoadPlayers : MonoBehaviour {

	AccountsManager theDAO;
	SceneNavigation theNavigator;
	public GameObject playerEntryPrefab;

	// Use this for initialization
	void Start () {
		theDAO = FindObjectOfType<AccountsManager> ();
		theNavigator = FindObjectOfType<SceneNavigation> ();
		theDAO.LoadExistingPlayers ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowPlayersToScreen (ArrayList players) {
		
		for (int i = 0; i<players.Count; i++) {
			//Instantiating the player entries
			GameObject gameObj = (GameObject)Instantiate (playerEntryPrefab);
			gameObj.transform.SetParent (this.transform);

			//Loading the players fromth the Arraylist
			ExistingPlayer tempObj = (ExistingPlayer)players [i];
			//Debug.Log (tempObj.email);
			gameObj.transform.FindChild("FirstNameHeader").GetComponent<Text>().text = tempObj.firstName;
			gameObj.transform.FindChild("LastNameHeader").GetComponent<Text>().text = tempObj.lastName;
			gameObj.transform.FindChild ("Button").FindChild ("ID").GetComponent<Text> ().text = tempObj.id;
			Button btn = gameObj.transform.FindChild ("Button").GetComponent<Button> ();
			btn.onClick.AddListener (SelectPlayer);
		
		}

	}

	public void SelectPlayer () {
		var button = EventSystem.current.currentSelectedGameObject;
		//button.transform.SetParent(this.transform);
		string id = button.transform.FindChild ("ID").GetComponent<Text> ().text;
		//Debug.Log (id);
		PlayerPrefs.SetString("playerID", id);
		theNavigator.GoToMainMenu ();
	}

}
