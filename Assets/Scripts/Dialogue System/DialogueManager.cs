﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {
	
	public GameObject textBox;
	public Text theText;
	public TextAsset textFile;
	public string[] textLines;
	public int currentLine;
	public int endAtLine;
	public PlayerController player;
	public bool isActive;
	public bool freezePlayer;

	public int[] playerLines; //Lines the player says
	public int[] otherLines; //Lines the side character says
	public int[] optionLines; //Lines that activates otions and stops the conversation
	public int[] options; //Lines that include the options displayed on buttons
	public int[] branches; //Lines you go to based on the options selected in the conversation
	public int[] killerLines; //The lines that just kill conversations (they end conversations)

	public int[] choiceIDs = new int[4];

	public bool continueConversation;

	public Button[] buttonsArray = new Button[4];
	public Text[] buttonsTextArray = new Text[4];

	public int[] selectedBranch = new int[4];

	// Use this for initialization
	void Start () {

		player = FindObjectOfType<PlayerController> ();

		if (textFile != null) {
			textLines = (textFile.text.Split('\n'));

		}

		if (endAtLine == 0) {
			endAtLine = textLines.Length - 1;
		}

		if (isActive) {
			
			EnableTextBox ();

		} else {
			
			DisableTextBox ();
			Debug.Log ("Disabled from isActive");

		}
		continueConversation = true;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (!isActive) {
			return;
		}

		ColorifyConversation ();

		theText.text = textLines[currentLine];
		if (Input.GetKeyDown (KeyCode.Return) && continueConversation) {
			currentLine += 1;
		}

//		for (int i = 0; i < killerLines.Length; i++) { //Searching for killer lines to end the conversation
//			if (currentLine == killerLines [i]+1) {
//				DisableTextBox ();
//				Debug.Log ("Disabled from a killer line");
//			}
//		}

		for (int i = 0; i<optionLines.Length; i++) { //Searching for a line that activates options
			
			if (optionLines[i] == currentLine) {
				
				//Loading the options onto the screen
				int numberOfOptions = int.Parse(textLines[currentLine+1]);

				for (int j = 0; j < 2*numberOfOptions; j+=2) { //(2*) because every option has an extra line showing its branch
					buttonsArray[j-(j/2)].gameObject.SetActive (true);
					buttonsTextArray[j-(j/2)].text = textLines[currentLine+j+2];
					choiceIDs [j - (j / 2)] = currentLine + 3 + j;
					//Debug.Log (choiceIDs[j - (j / 2)]);
					selectedBranch [j-(j/2)] = int.Parse (textLines[currentLine+j+3]); //Saving branches addresses

				}
				//Debug.Log ("OptionLines Added");
				continueConversation = false;
			}

		}

		if (currentLine > endAtLine) {
			DisableTextBox ();
			Debug.Log ("Disabled from the endline");
		}			

	}

	public void EnableTextBox () {
		textBox.SetActive (true);
		isActive = true;
		if (freezePlayer) {
			player.canMove = false;
		}
	}

	public  void DisableTextBox () {
		textBox.SetActive (false);
		isActive = false;
		player.canMove = true;
	}

	public void ReloadScript (TextAsset activatedText) {
		if (activatedText != null) {
			textLines = new string[1];
			textLines = (activatedText.text.Split('\n'));
			//theText.color = new Color(204.0f/255.0f, 226.0f/255.0f, 147.0f/255.0f);
		}
	}

	public void ColorifyConversation () {
		
		for(int i=0; i<playerLines.Length; i++) {
			if (playerLines [i] == currentLine) {
				theText.color = new Color(0.0f/255.0f, 0.0f/255.0f, 0.0f/255.0f);
			}
		}

		for(int i=0; i<otherLines.Length; i++) {
			if (otherLines [i] == currentLine) {
				theText.color = new Color(204.0f/255.0f, 226.0f/255.0f, 147.0f/255.0f);
			}
		}

	}

	public void Option1Press () {
		currentLine = selectedBranch [0]-1; //(-1) because currentLine loops through 0-index arrays
		//player.choicesToSave.Add(currentLine+1); //Saving the choice temporarily until a checkpoint is reached, then saved to the local DB
		player.choicesToSave.Add(choiceIDs[0]);
		continueConversation = true;
		buttonsArray[0].gameObject.SetActive (false);
		buttonsArray[1].gameObject.SetActive (false);
		buttonsArray[2].gameObject.SetActive (false);
		buttonsArray[3].gameObject.SetActive (false);
	}

	public void Option2Press () {
		currentLine = selectedBranch [1]-1; //(-1) because currentLine loops through 0-index arrays
		//player.choicesToSave.Add(currentLine+1); //Saving the choice temporarily until a checkpoint is reached, then saved to the local DB
		player.choicesToSave.Add(choiceIDs[1]);
		continueConversation = true;
		buttonsArray[0].gameObject.SetActive (false);
		buttonsArray[1].gameObject.SetActive (false);
		buttonsArray[2].gameObject.SetActive (false);
		buttonsArray[3].gameObject.SetActive (false);
	}

	public void Option3Press () {
		currentLine = selectedBranch [2]-1; //(-1) because currentLine loops through 0-index arrays
		//player.choicesToSave.Add(currentLine+1); //Saving the choice temporarily until a checkpoint is reached, then saved to the local DB
		player.choicesToSave.Add(choiceIDs[2]);
		continueConversation = true;
		buttonsArray[0].gameObject.SetActive (false);
		buttonsArray[1].gameObject.SetActive (false);
		buttonsArray[2].gameObject.SetActive (false);
		buttonsArray[3].gameObject.SetActive (false);
	}

	public void Option4Press () {
		currentLine = selectedBranch [3]-1; //(-1) because currentLine loops through 0-index arrays
		//player.choicesToSave.Add(currentLine+1); //Saving the choice temporarily until a checkpoint is reached, then saved to the local DB
		player.choicesToSave.Add(choiceIDs[3]);
		continueConversation = true;
		buttonsArray[0].gameObject.SetActive (false);
		buttonsArray[1].gameObject.SetActive (false);
		buttonsArray[2].gameObject.SetActive (false);
		buttonsArray[3].gameObject.SetActive (false);
	}

	public void NextLine () {
		Debug.Log ("Touched");
		if (continueConversation) {
			currentLine += 1;
		}
	}
}
