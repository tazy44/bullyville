﻿using System;
using System.Collections.Generic;
using System.Collections;
using NUnit.Framework;
using UnityEngine;



[TestFixture]
public class ALevelManagerTest {

	[Test]
	public void Test00CoinsHeadStart () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		Assert.AreEqual (theLevelManager.coinCount, 0);
	}

	[Test]
	public void Test01HealthHeadStart () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		Assert.AreEqual (theLevelManager.maxHealth, 6);
	}

	[Test]
	public void Test02HealthCount () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		Assert.AreEqual (theLevelManager.healthCount, 0);
	}

	[Test]
	public void Test03LivesHeadStart () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		Assert.AreEqual (theLevelManager.startingLives, 3);
	}

	[Test]
	public void Test04LoadedCoins () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		Assert.AreEqual (theLevelManager.loadedCoins, 0);
	}

	[Test]
	public void Test05LoadedHealth () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		Assert.AreEqual (theLevelManager.loadedhealth, 0);
	}

	[Test]
	public void Test06LoadedLives () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		Assert.AreEqual (theLevelManager.loadedLives, 0);
	}

	[Test]
	public void Test07CurrentLives () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		Assert.AreEqual (theLevelManager.currentLives, 0);
	}

	[Test]
	public void Test08Respawning () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		Assert.AreEqual (theLevelManager.respawning, false);
	}

	[Test]
	public void Test09Invincible () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		Assert.AreEqual (theLevelManager.invincible, false);
	}

	[Test]
	public void Test10Once () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		Assert.AreEqual (theLevelManager.once, true);
	}

	[Test]
	public void Test11AddCoins () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		theLevelManager.addCoins (4);
		Assert.AreEqual (theLevelManager.coinCount, 4);
	}

	[Test]
	public void Test12GiveHelath () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		theLevelManager.GiveHealth (2);
		Assert.AreEqual (theLevelManager.healthCount, 2);
	}

	[Test]
	public void Test13AddLives () {

		LevelManager theLevelManager = GameObject.FindObjectOfType<LevelManager> ();
		theLevelManager.AddLives (1);
		Assert.AreEqual (theLevelManager.currentLives, 1);
	}

}

[TestFixture]
public class BPlayerControllerTest {

	[Test]
	public void Test14MoveSpeed () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.moveSpeed, 3.0f);
	}

	[Test]
	public void Test15JumpSpeed () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.jumpSpeed, 10.0f);
	}

	[Test]
	public void Test16CheckGroundRadius () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.checkGroundRadius, 0.600000024f);
	}

	[Test]
	public void Test17IsGrounded () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.isGrounded, false);
	}

	[Test]
	public void Test18KnockbackForce () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.knockbackForce, 5.0f);
	}

	[Test]
	public void Test19KnockbackLength () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.knockbackLength, 0.200000003f);
	}

	[Test]
	public void Test20KnockbackCounter () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.knockbackCounter, 0.0f);
	}

	[Test]
	public void Test21CanMove () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.canMove, true);
	}

	[Test]
	public void Test22LoadedX () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.loadedX, 0f);
	}

	[Test]
	public void Test23LoadedY () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.loadedY, 0f);
	}

	[Test]
	public void Test23LoadedZ () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.loadedZ, 0f);
	}

	[Test]
	public void Test24PlayerIDLength () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.playerID.Length, 0);
	}

	[Test]
	public void Test25PlayerEmailLength () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.playerEmail.Length, 0);
	}

	[Test]
	public void Test26PlayerID () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.playerID, "");
	}

	[Test]
	public void Test27PlayerEmail () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.playerEmail, "");
	}

	[Test]
	public void Test28ChoicesToSave () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.choicesToSave.Capacity, 0);
	}

	[Test]
	public void Test29Die () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.die, false);
	}

	[Test]
	public void Test30Once () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.once, false);
	}

	[Test]
	public void Test31XToSave () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.xToSave, 0f);
	}

	[Test]
	public void Test32YToSave () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.yToSave, 0f);
	}

	[Test]
	public void Test33ZToSave () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.zToSave, 0f);
	}

	[Test]
	public void Test34CoinsToSave () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.coinsToSave, 0);
	}

	[Test]
	public void Test35HealthToSave () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.healthToSave, 0);
	}

	[Test]
	public void Test36livesToSave () {

		PlayerController thePlayerController = GameObject.FindObjectOfType<PlayerController> ();
		Assert.AreEqual (thePlayerController.livesToSave, 0);
	}


}

[TestFixture]
public class CAccountManagerTest {

//	[Test]
//	public void Test37Email () {
//
//		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
//		Assert.AreEqual (theAccountManager.email, null);
//	}
//
//	[Test]
//	public void Test38Password () {
//
//		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
//		Assert.AreEqual (theAccountManager.password, null);
//	}
//
//	[Test]
//	public void Test39FirstName () {
//
//		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
//		Assert.AreEqual (theAccountManager.firstName, null);
//	}
//
//	[Test]
//	public void Test40LastName () {
//
//		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
//		Assert.AreEqual (theAccountManager.lastName, null);
//	}

	[Test]
	public void Test41Age () {

		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
		Assert.AreEqual (theAccountManager.age, 0);
	}

	[Test]
	public void Test42PositionX () {

		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
		Assert.AreEqual (theAccountManager.positionX, 0f);
	}

	[Test]
	public void Test43PositionY () {

		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
		Assert.AreEqual (theAccountManager.positionY, 0f);
	}

	[Test]
	public void Test44PostionZ () {

		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
		Assert.AreEqual (theAccountManager.positionZ, 0f);
	}

	[Test]
	public void Test45Coins () {

		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
		Assert.AreEqual (theAccountManager.coins, 0);
	}

	[Test]
	public void Test46Helath () {

		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
		Assert.AreEqual (theAccountManager.health, 0);
	}

	[Test]
	public void Test47Lives () {

		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
		Assert.AreEqual (theAccountManager.lives, 0);
	}

	[Test]
	public void Test48ConnectionString () {

		AccountsManager theAccountManager = GameObject.FindObjectOfType<AccountsManager> ();
		Assert.AreEqual (theAccountManager.connectionString, "");
	}

}

[TestFixture]
public class DTextActivationTest {

	[Test]
	public void Test49StartLine () {

		ActivateTextAtLine theTextActivator = GameObject.FindObjectOfType<ActivateTextAtLine> ();
		Assert.AreEqual (theTextActivator.startLine, 79);
	}

	[Test]
	public void Test50DestroyWhenFinished () {

		ActivateTextAtLine theTextActivator = GameObject.FindObjectOfType<ActivateTextAtLine> ();
		Assert.AreEqual (theTextActivator.destroyWhenFinished, true);
	}

	[Test]
	public void Test51RequireButtonPress () {

		ActivateTextAtLine theTextActivator = GameObject.FindObjectOfType<ActivateTextAtLine> ();
		Assert.AreEqual (theTextActivator.requireButtonPress, false);
	}

	[Test]
	public void Test52WaitForPress () {

		ActivateTextAtLine theTextActivator = GameObject.FindObjectOfType<ActivateTextAtLine> ();
		Assert.AreEqual (theTextActivator.waitForPress, false);
	}
		
}

[TestFixture]
public class ECameraControlTest {

	[Test]
	public void Test53FollowAhead () {

		CameraControl theCameraController = GameObject.FindObjectOfType<CameraControl> ();
		Assert.AreEqual (theCameraController.followAhead, 3.0f);
	}

	[Test]
	public void Test54Smoothing () {

		CameraControl theCameraController = GameObject.FindObjectOfType<CameraControl> ();
		Assert.AreEqual (theCameraController.smoothing, 1.0f);
	}

}

[TestFixture]
public class FCheckpointTest {

	[Test]
	public void Test55CheckPointActivity () {

		CheckpointController theCheckPointController = GameObject.FindObjectOfType<CheckpointController> ();
		Assert.AreEqual (theCheckPointController.checkpointActive, false);
	}

	[Test]
	public void Test56TreeY () {

		CheckpointController theCheckPointController = GameObject.FindObjectOfType<CheckpointController> ();
		Assert.AreEqual (theCheckPointController.treeY, false);
	}

}

[TestFixture]
public class GCoinTest {

	[Test]
	public void Test57CoinValue () {

		Coin theCoin = GameObject.FindObjectOfType<Coin> ();
		Assert.AreEqual (theCoin.coinValue, 1);
	}
}

[TestFixture]
public class HHealthTest {

	[Test]
	public void Test58HealthToGive () {

		HealthPickup theHealth = GameObject.FindObjectOfType<HealthPickup> ();
		Assert.AreEqual (theHealth.healthToGive, 2);
	}
}

[TestFixture]
public class IDamageTest {

	[Test]
	public void Test59HurtPlayer () {

		HurtPlayer theHurt = GameObject.FindObjectOfType<HurtPlayer> ();
		Assert.AreEqual (theHurt.damageToGive, 1);
	}
}

[TestFixture]
public class JLifeTest {

	[Test]
	public void Test60Life () {

		Life theLife = GameObject.FindObjectOfType<Life> ();
		Assert.AreEqual (theLife.livesToGive, 1);
	}
}

[TestFixture]
public class KMovingObjectTest {

	[Test]
	public void Test61MovingObject () {

		MovingObject theObject = GameObject.FindObjectOfType<MovingObject> ();
		Assert.AreEqual (theObject.moveSpeed, 3.0f);
	}
}

[TestFixture]
public class LPatrollingEnemyTest {

	[Test]
	public void Test62MovingRight () {

		PatrollingEnemy theEnemy = GameObject.FindObjectOfType<PatrollingEnemy> ();
		Assert.AreEqual (theEnemy.movingRight, false);
	}

	[Test]
	public void Test63MoveSpeed () {

		PatrollingEnemy theEnemy = GameObject.FindObjectOfType<PatrollingEnemy> ();
		Assert.AreEqual (theEnemy.moveSpeed, 3.0f);
	}
}

[TestFixture]
public class MStompTest {

	[Test]
	public void Test64MovingRight () {

		StompEnemy theEnemy = GameObject.FindObjectOfType<StompEnemy> ();
		Assert.AreEqual (theEnemy.bounceForce, 7.0f);
	}
		
}




