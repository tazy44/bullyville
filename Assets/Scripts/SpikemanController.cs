﻿using UnityEngine;
using System.Collections;

public class SpikemanController : MonoBehaviour {

	public float moveSpeed;
	private bool canMove;

	private Rigidbody2D myRigidbody;

	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (canMove) {
			myRigidbody.velocity = new Vector3 (-moveSpeed, myRigidbody.velocity.y, 0f);
		}
	}

	void OnBecameVisible(){ //On becoming visible to the camera
		canMove = true;
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "DeathZone") {
			//Destroy (gameObject);
			gameObject.SetActive(false);
		}
	}

	void OnEnable(){
		canMove = false; //So that it's only true when it becomes visible in our camera
	}
}
