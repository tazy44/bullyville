﻿using UnityEngine;
using System.Collections;

public class DataSender : MonoBehaviour {

	public string username;
	public string choiceNumber;

	// Use this for initialization
	void Start () {
		SendChoice (username, choiceNumber);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SendChoice (string username, string choiceNumber) {
		WWWForm form = new WWWForm ();
		form.AddField ("usernamePost", username);
		form.AddField ("choiceNumberPost", choiceNumber);
		WWW poster = new WWW ("http://localhost/BullyVille/insert.php", form);
	}
}
