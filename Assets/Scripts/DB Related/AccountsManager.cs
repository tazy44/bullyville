﻿using UnityEngine;
using System.Collections;
using System;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine.UI;

public class AccountsManager : MonoBehaviour {

	SceneNavigation theNavigator;

	public string email;
	public string password;
	public string firstName;
	public string lastName;
	public int age;

	public InputField emailField;
	public InputField passwordField;
	public InputField FNField;
	public InputField LNField;
	public InputField AgeField;

	//Loaded varibles that hold the progress of the player
	public float positionX = 0.0f;
	public float positionY = 0.0f;
	public float positionZ = 0.0f;
	public int coins = 0;
	public int health = 0;
	public int lives = 0;

	public string connectionString;
	public RESTManager theRESTManager;
	public LoadPlayers theLoader;
	// Use this for initialization
	void Start () {
		theNavigator = FindObjectOfType<SceneNavigation> ();
		theRESTManager = FindObjectOfType<RESTManager> ();
		theLoader = FindObjectOfType<LoadPlayers> ();
		connectionString = "URI=file:" + Application.dataPath + "/bvdb.sqlite";
		//GetData ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void GetData () {
		
		using (IDbConnection dbConnection = new SqliteConnection (connectionString)) {
			dbConnection.Open ();

			using (IDbCommand dbCmd = dbConnection.CreateCommand()) {
				string sqlQuery = "SELECT*FROM player";
				dbCmd.CommandText = sqlQuery;

				using (IDataReader reader = dbCmd.ExecuteReader()) {
					while (reader.Read()) {
						Debug.Log (reader.GetString(3) + " of Age " + reader.GetInt32(5));
					}
					dbConnection.Close ();
					reader.Close ();
				}

			}

		}

	}

	public string GenerateID () {
		string x = System.Guid.NewGuid ().ToString ();
		string y = System.DateTime.Now.Ticks.ToString ();
		return x + y;
	}

	public void addAccount () {

		string playerID;

		//Loading values from Input Fields into variables
		email = emailField.text.ToString ();
		password = passwordField.text.ToString ();
		firstName = FNField.text.ToString();
		lastName = LNField.text.ToString ();
		string ageText = AgeField.text.ToString ();
		int.TryParse (ageText, out age);

		//Making sure all fields are filled
		if (email.Length == 0) {
			emailField.placeholder.color = new Color(223.0f/255.0f, 41.0f/255.0f, 41.0f/255.0f);
			return;
		}
		if (password.Length == 0) {
			passwordField.placeholder.color = new Color(223.0f/255.0f, 41.0f/255.0f, 41.0f/255.0f);
			return;
		}
		if (firstName.Length == 0) {
			FNField.placeholder.color = new Color(223.0f/255.0f, 41.0f/255.0f, 41.0f/255.0f);
			return;
		}
		if (lastName.Length == 0) {
			LNField.placeholder.color = new Color(223.0f/255.0f, 41.0f/255.0f, 41.0f/255.0f);
			return;
		}
		if (age == 0) {
			AgeField.placeholder.color = new Color(223.0f/255.0f, 41.0f/255.0f, 41.0f/255.0f);
			return;
		}

		//Send Data (without local ID) to Cloud & bring "Cloud_ID"
		playerID = GenerateID();
		theRESTManager.SendNewAccountToCloud(firstName, lastName, age, email, password, playerID);

		//Connecting with the local DB
		using (IDbConnection dbConnection = new SqliteConnection (connectionString)) {
			dbConnection.Open ();
			//Inserting new player data into the local DB
			using (IDbCommand dbCmd = dbConnection.CreateCommand()) {
				string sqlQuery = string.Format("INSERT INTO player(Email, Password, First_Name, Last_Name, Age, ID)" +
					" VALUES (\"{0}\", \"{1}\", \"{2}\", \"{3}\", \"{4}\", \"{5}\")", email, password, firstName, lastName, age, playerID);
				dbCmd.CommandText = sqlQuery;
				dbCmd.ExecuteScalar ();
				//dbConnection.Close ();
				Debug.Log ("successfully added " + firstName + " whose age is " + age);
			}
				
			//Inserting the initial progress of the new player into the local DB
			using (IDbCommand dbCmd = dbConnection.CreateCommand()) {
				string sqlQuery = string.Format("INSERT INTO progress(Progress_ID, X_Position, Y_Position, Z_Position," +
					" Coins, Health, Lives) VALUES (\"{0}\", \"{1}\", \"{2}\", \"{3}\", \"{4}\", \"{5}\", \"{6}\");", playerID, 0.0, 0.0, 0.0, 0, 0, 0);
				dbCmd.CommandText = sqlQuery;
				dbCmd.ExecuteScalar ();
				//dbConnection.Close ();
				Debug.Log ("Initial progress created for ID " + playerID);
			}

			dbConnection.Close ();
		}
		//Transfering the ID & Email through different scenes
		PlayerPrefs.SetString("playerID", playerID);
		theNavigator.GoToMainMenu ();
		//SceneManager.LoadScene ("Main Menu");

	}

	public void LoadExistingPlayers () {

		ArrayList players = new ArrayList();

		//Connecting with the local DB
		using (IDbConnection dbConnection = new SqliteConnection (connectionString)) {
			dbConnection.Open ();
			//Fetching the current Player's ID from the local DB
			using (IDbCommand dbCmd = dbConnection.CreateCommand()) {
				string sqlQuery = string.Format("SELECT*FROM player");
				dbCmd.CommandText = sqlQuery;

				using (IDataReader reader = dbCmd.ExecuteReader()) {
					while (reader.Read()) {
						//Debug.Log (reader.GetString(1));
						ExistingPlayer x = new ExistingPlayer(reader.GetString(0), reader.GetString(1), reader.GetString(2), 
							reader.GetString(3), reader.GetString(4), reader.GetInt32(5));
						players.Add (x);
					}
					reader.Close ();
				}

			}
			theLoader.ShowPlayersToScreen (players);
			dbConnection.Close ();
			players.Clear ();
		}
	}

	//LoadProgress is called twice (once in the PlayerController to load the position & another in the LevelManager to load coins, heakth & lives)
	public void LoadProgress (string progressID) {
		
		//Connecting with the local DB
		using (IDbConnection dbConnection = new SqliteConnection (connectionString)) {
			dbConnection.Open ();
			//Fetching the current Player's ID from the local DB
			using (IDbCommand dbCmd = dbConnection.CreateCommand()) {
				string sqlQuery = string.Format("SELECT*FROM progress WHERE Progress_ID = \"{0}\"", progressID);
				dbCmd.CommandText = sqlQuery;

				using (IDataReader reader = dbCmd.ExecuteReader()) {
					while (reader.Read()) {
						positionX = reader.GetFloat (1);
						positionY = reader.GetFloat (2);
						positionZ = reader.GetFloat (3);
						coins = reader.GetInt32 (4);
						health = reader.GetInt32 (5);
						lives = reader.GetInt32 (6);
					}
					//dbConnection.Close ();
					reader.Close ();
				}

			}

			dbConnection.Close ();
		}
	}

	//To save position, coins, health and lives only and called one time only in PlayerController
	public void SaveProgress (string progressID, float x, float y, float z, int coins, int health, int lives) {

			//Connecting with the local DB
			using (IDbConnection dbConnection = new SqliteConnection (connectionString)) {
				dbConnection.Open ();
							
				//Inserting the player's progress into the local DB
				using (IDbCommand dbCmd = dbConnection.CreateCommand()) {
					string sqlQuery = string.Format("UPDATE progress SET X_Position = \"{0}\", Y_Position = \"{1}\", Z_Position = \"{2}\"," +
					" Coins = \"{3}\", Health = \"{4}\", Lives = \"{5}\" WHERE Progress_ID = \"{6}\";", x, y, z, coins, health, lives, progressID);
					dbCmd.CommandText = sqlQuery;
					dbCmd.ExecuteScalar ();
					//dbConnection.Close ();
				}

				dbConnection.Close ();
			}

	}

	public void SaveChoicesProgress (string playerID, int choiceID) {

		//Connecting with the local DB
		using (IDbConnection dbConnection = new SqliteConnection (connectionString)) {
			dbConnection.Open ();

			//Inserting the player's progress into the local DB
			using (IDbCommand dbCmd = dbConnection.CreateCommand()) {
				string sqlQuery = string.Format("INSERT INTO choices(Player_ID, Choice_ID) VALUES(\"{0}\", \"{1}\")", playerID, choiceID);
				dbCmd.CommandText = sqlQuery;
				dbCmd.ExecuteScalar ();
				//dbConnection.Close ();
			}

			dbConnection.Close ();
		}

	}

	//Fetching all choices from the local DB 
	public ArrayList FetchAllChoices (string playerID) {
		ArrayList choices = new ArrayList ();

		//Connecting with the local DB
		using (IDbConnection dbConnection = new SqliteConnection (connectionString)) {
			dbConnection.Open ();

			using (IDbCommand dbCmd = dbConnection.CreateCommand()) {
				string sqlQuery = string.Format("SELECT Choice_ID FROM choices WHERE Player_ID = \"{0}\"", playerID);
				dbCmd.CommandText = sqlQuery;

				int i = 0;
				int x = 0;
				using (IDataReader reader = dbCmd.ExecuteReader()) {
					while (reader.Read()) {
						x = reader.GetInt32 (0);
						choices.Add (x);
						i++;
					}
					reader.Close ();
					i = 0;
					x = 0;
				}

			}

			dbConnection.Close ();
		}
		return choices;
	}

	public void RemoveAllChoices (string playerID) {

		//Connecting with the local DB
		using (IDbConnection dbConnection = new SqliteConnection (connectionString)) {
			dbConnection.Open ();

			//Inserting the player's progress into the local DB
			using (IDbCommand dbCmd = dbConnection.CreateCommand()) {
				string sqlQuery = string.Format("DELETE FROM choices WHERE Player_ID = \"{0}\"", playerID);
				dbCmd.CommandText = sqlQuery;
				dbCmd.ExecuteScalar ();
				//dbConnection.Close ();
			}

			dbConnection.Close ();
		}

	}

	public void RemoveAllProgress (string playerID) {

		//Connecting with the local DB
		using (IDbConnection dbConnection = new SqliteConnection (connectionString)) {
			dbConnection.Open ();

			//Inserting the player's progress into the local DB
			using (IDbCommand dbCmd = dbConnection.CreateCommand()) {
				string sqlQuery = string.Format("UPDATE progress SET X_Position = \"{0}\", Y_Position = \"{1}\", Z_Position = \"{2}\"," +
					" Coins = \"{3}\", Health = \"{4}\", Lives = \"{5}\" WHERE Progress_ID = \"{6}\";",0.0, 0.0, 0.0, 0, 0, 0, playerID);
				dbCmd.CommandText = sqlQuery;
				dbCmd.CommandText = sqlQuery;
				dbCmd.ExecuteScalar ();
				//dbConnection.Close ();
			}

			dbConnection.Close ();
		}

	}

}
