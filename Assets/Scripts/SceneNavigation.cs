﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneNavigation : MonoBehaviour {

	public AudioSource ClickEffect;
	int playerID;

	// Use this for initialization
	void Start () {
		playerID = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	//For the Main menu Scene
	public void NewGame() {
		ClickEffect.Play ();
		SceneManager.LoadScene ("Level1");
		playerID =  PlayerPrefs.GetInt ("playerID");
		Debug.Log ("PlayID " + playerID);
	}

	public void ContinueGame() {
		ClickEffect.Play ();
		//SceneManager.LoadScene (levelSelect);
	}

	public void NewPlayer() {
		ClickEffect.Play ();
		SceneManager.LoadScene ("NewAccount");
	}

	public void ExistingPlayer() {
		ClickEffect.Play ();
		SceneManager.LoadScene ("ExistingSelection");
	}

	public void BackToAccountSelection() {
		ClickEffect.Play ();
		SceneManager.LoadScene ("Account Selection");
	}

	public void GoToMainMenu() {
		ClickEffect.Play ();
		SceneManager.LoadScene ("Main Menu");
		playerID =  PlayerPrefs.GetInt ("playerID");
		//Debug.Log ("PlayID " + playerID);
	}

	public void Settings () {
		ClickEffect.Play ();
	
	}

	public void QuitGame () {
		ClickEffect.Play ();
		Application.Quit ();
	}
		

}
