﻿using UnityEngine;
using System.Collections;

public class CheckpointController : MonoBehaviour {

	public Sprite treeShort;
	public Sprite treeTall;
	private SpriteRenderer theSpriteRenerer;
	public bool checkpointActive;
	public bool treeY;

	// Use this for initialization
	void Start () {
		theSpriteRenerer = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.tag == "Player") {
			theSpriteRenerer.sprite = treeTall;
			if (treeY != true) { //Makes sure the tree doesn't keep changing posion every time it's reached.
				transform.position = new Vector3 (transform.position.x, transform.position.y + 0.3f, 0f);
				treeY = true;
			}
			checkpointActive = true;
			//Debug.Log ("Checkpoint", gameObject);
		}
	}
}
